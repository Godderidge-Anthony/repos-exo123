package testudptcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class Client {
	public static void main(String[] args) throws IOException {
		SocketAddress host = new InetSocketAddress("localhost",8080);
		Socket c = new Socket();
		c.connect(host,8080);
		InputStream is = c.getInputStream();
		int x = is.read();
		System.out.println("x = "+ x);
		OutputStream os = c.getOutputStream();
		os.write(x+1);
		x = is.read();
		System.out.println("x= "+x);
		
		c.close();
	}
}
